

import UIKit
import EasyTipView
import SwiftyJSON

var baseURL      = ""
var chkVal       = ""
var deviceType   = "i"
var SchoolID     = "0"
var devToken     = "789151645671871"


let appName = Bundle.main.infoDictionary!["CFBundleName"] as! String




let screenHeight = UIScreen.main.bounds.size.height
let screenWidth = UIScreen.main.bounds.size.width
var defaults = UserDefaults.standard

//Local variables
var localUserID                 = ""
var localUserAddress            = ""
var localUserName               = ""

//******************************************************************
// MARK: - Workaround for the Xcode 11.2 bug
//******************************************************************
class UITextViewWorkaround: NSObject {

    // --------------------------------------------------------------------
    // MARK: Singleton
    // --------------------------------------------------------------------
    // make it a singleton
    static let unique = UITextViewWorkaround()

    // --------------------------------------------------------------------
    // MARK: executeWorkaround()
    // --------------------------------------------------------------------
    func executeWorkaround() {

        if #available(iOS 13.2, *) {

            NSLog("UITextViewWorkaround.unique.executeWorkaround(): we are on iOS 13.2+ no need for a workaround")

        } else {

            // name of the missing class stub
            let className = "_UITextLayoutView"

            // try to get the class
            var cls = objc_getClass(className)

            // check if class is available
            if cls == nil {

                // it's not available, so create a replacement and register it
                cls = objc_allocateClassPair(UIView.self, className, 0)
                objc_registerClassPair(cls as! AnyClass)

                #if DEBUG
                NSLog("UITextViewWorkaround.unique.executeWorkaround(): added \(className) dynamically")
               #endif
           }
        }
    }
}

@IBDesignable class PaddingLabel: UILabel {
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}


func addTextOnImageView(_ imageView:UIImageView,text:String,font:CGFloat) -> UIImage
{
    UIGraphicsBeginImageContextWithOptions(CGSize(width: imageView.bounds.size.width, height: imageView.bounds.size.height), false, 0)
    
    let maxTextRect = CGRect(x: 0, y: 0, width: imageView.bounds.size.width, height: imageView.bounds.size.height);
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = .center
    
    let attrs = [NSAttributedString.Key.font: UIFont(name: "Noteworthy-Light", size: font)!,NSAttributedString.Key.paragraphStyle: paragraphStyle,NSAttributedString.Key.foregroundColor:lightGrayColor]
    
    let string:NSMutableString = NSMutableString(string: text)
    let actualRect =  string.boundingRect(with: maxTextRect.size, options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
    
    
    let drawRect = CGRect(x: maxTextRect.minX + ((maxTextRect.width - actualRect.width) * 0.5)
        , y: maxTextRect.minY + ((maxTextRect.height - actualRect.height) * 0.5)
        , width: actualRect.width
        , height: actualRect.height)
    string.draw(with: drawRect, options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
    let img = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    // 7
    return img!
    
}

func setTintImage(_ tintImage:UIImageView, image:UIImage)
{
    tintImage.image = image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    tintImage.tintColor = ImageIconTintColor
}

func setTintImageWithColor(_ tintImage:UIImageView, image:UIImage, color:UIColor)
{
    tintImage.image = image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    tintImage.tintColor = color
}

func setTintButtonImage(_ tintImage:UIButton, image:UIImage, color:UIColor)
{
    tintImage.setImage(image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: UIControl.State())
    tintImage.tintColor = color
}


func updateLabelTextFont(_ sk_label:UILabel) {
    if (sk_label.text!.isEmpty || sk_label.bounds.size.equalTo(CGSize.zero)) {
        return;
    }
    
    let textViewSize = sk_label.bounds.size;
    let fixedWidth = textViewSize.width-20;
    let expectSize = sk_label.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)));
    
    var expectFont = sk_label.font;
    if (expectSize.height > textViewSize.height) {
        while (sk_label.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height > textViewSize.height) {
            expectFont = sk_label.font!.withSize(sk_label.font!.pointSize - 1)
            sk_label.font = expectFont
        }
    }
    else {
        while (sk_label.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height < textViewSize.height) {
            expectFont = sk_label.font;
            sk_label.font = sk_label.font!.withSize(sk_label.font!.pointSize + 1)
        }
        sk_label.font = expectFont;
    }
}


import SystemConfiguration
//let hasInternet = connectedToNetwork()
func connectedToNetwork() -> Bool {
    
    
    
    var zeroAddress = sockaddr_in()
    
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags : SCNetworkReachabilityFlags = []
    
    if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
        
        return false
        
    }
    
    let isReachable = flags.contains(.reachable)
    
    let needsConnection = flags.contains(.connectionRequired)
    
    return (isReachable && !needsConnection)
    
}
func setImageBorder(_ sView:UIImageView)
{
    sView.layer.borderColor = UIColor.gray.cgColor
    sView.layer.borderWidth = 1
    sView.layer.masksToBounds = true
    sView.layer.cornerRadius = sView.frame.size.width/2
}
func setButtonBorder(_ sView:UIButton)
{
    sView.layer.borderColor = UIColor.gray.cgColor
    sView.layer.borderWidth = 1
    sView.layer.masksToBounds = true
    sView.layer.cornerRadius = sView.frame.size.width/2
}
func setTableViewBorder(_ sView:UITableView)
{
    sView.layer.borderColor = UIColor.white.cgColor
    sView.layer.borderWidth = 1
    sView.layer.cornerRadius = 5
    
    sView.layer.shadowColor = UIColor.black.cgColor
    sView.layer.shadowOffset = CGSize.zero
    sView.layer.shadowOpacity = 0.5
    sView.layer.shadowRadius = 2
}
func setViewBorder(_ sView:UIView)
{
    sView.layer.borderColor = UIColor.white.cgColor
    sView.layer.borderWidth = 1
    sView.layer.cornerRadius = 5
    
    sView.layer.shadowColor = UIColor.black.cgColor
    sView.layer.shadowOffset = CGSize.zero
    sView.layer.shadowOpacity = 0.5
    sView.layer.shadowRadius = 2
}
extension CALayer {
    
    func addBorder(_ edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        print(edge)
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
}

private var kAssociationKeyNextField: UInt8 = 0

extension UITextField {
    var nextField: UITextField? {
        get {
            return objc_getAssociatedObject(self, &kAssociationKeyNextField) as? UITextField
        }
        set(newField) {
            objc_setAssociatedObject(self, &kAssociationKeyNextField, newField, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}
//set textfiled next line by line tab action
func TextFiledGoToNext(_ textFields:[UITextField])
{
    for index in 0...textFields.count-1
    {
        let firstTextField = textFields[index]
        var secondTextFiled = UITextField()
        if(index == textFields.count-1)
        {
            secondTextFiled = textFields[index]
        }else
        {
            secondTextFiled = textFields[index+1]
        }
        firstTextField.nextField = secondTextFiled
    }
}
/// custom alertView
func SKCustomeAlert(_ type:String, controller:UIViewController)
{
    var alertMessage = ""
    if(type == "ERROR")
    {
        alertMessage = "Loading failed, May be your network connection is lost!, Plaese try again"
    }else if(type == "NETWORK")
    {
        alertMessage = "Please connect your device with mobile data or wifi!"
    }else if(type == "VALERROR")
    {
        alertMessage = "Must required all fields!"
    }else
    {
        alertMessage = type
    }
    let appName = Bundle.main.infoDictionary!["CFBundleName"] as! String
    let alert=UIAlertController(title: appName, message: alertMessage, preferredStyle: UIAlertController.Style.alert);
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil));
    controller.present(alert, animated: true, completion: nil);
    //UIApplication.sharedApplication().windows[0].rootViewController?.presentViewController(alert, animated: true, completion: nil);
    
}

enum ErrorType: String {
    case FailedError = "ERROR"
    case NetworkError = "NETWORK"
    case ValidationError = "VALERROR"
}


func validateTextFieldWithRedBox(_ textField: UITextField){
    
    let str : CharacterSet = CharacterSet(charactersIn: " ")
    if( textField.text!.trimmingCharacters(in: str) == "")
    {
        chkVal = "YES"
        textField.layer.borderColor    = UIColor.red.cgColor
        textField.layer.borderWidth = 1
        //textField.linesWidth = 2.0
    }else{
        textField.layer.borderColor    = UIColor.clear.cgColor
        textField.layer.borderWidth = 0
    }
    
}

func validateTextFieldWithRedLine(_ textFields: [SKCustomTextField], color: UIColor) {
    for textField in textFields {
        let str : CharacterSet = CharacterSet(charactersIn: " ")
        if( textField.text!.trimmingCharacters(in: str) == "")
        {
            chkVal = "YES"
            textField.borderColor = UIColor.red
        }else{
            textField.borderColor = color
        }
    }
}

func isValidEmail(_ textField: UITextField) -> Bool {
    // println("validate calendar: \(testStr)")
    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//    if(!emailTest.evaluate(with: textField.text!))
//    {
//        var preferences = EasyTipView.globalPreferences
//        preferences.drawing.backgroundColor = AppCommonColor
//        // preferences.bubbleColor = UIColor ( red: 0.0, green: 0.4784, blue: 1.0, alpha: 0.8 )
//        EasyTipView.show(animated:true, forView: textField, withinSuperview: UINavigationController().navigationController?.view, text: "Enter Valid Email", preferences: preferences, delegate: nil)
//    }
    return emailTest.evaluate(with: textField.text!)
}
func validateTextField(_ textField: UITextField, errorMessage: String, show: Bool, withinSuperview superview : UIView?){
    
    let str : CharacterSet = CharacterSet(charactersIn: " ")
    if( textField.text!.trimmingCharacters(in: str) == "")
    {
        if(errorMessage != "" && show)
        {
            let preferences = EasyTipView.globalPreferences
            // preferences.bubbleColor = UIColor ( red: 0.0, green: 0.4784, blue: 1.0, alpha: 0.8 )
            EasyTipView.show(animated: true, forView: textField, withinSuperview: superview, text: "\(errorMessage)", preferences: preferences, delegate: nil)
        }
    }else{
        for view in (superview?.subviews)! {
            if let tipView = view as? EasyTipView {
                tipView.dismiss()
            }
        }
    }
    
    if(textField.text!.trimmingCharacters(in: str) == "" && chkVal == "")
    {
        chkVal = "\(errorMessage)"
    }
    
}

func phoneNumberValidate(_ value: String) -> Bool {
    
    let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
    
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    
    let result =  phoneTest.evaluate(with: value)
    
    return result
    
}
func validateTextFieldWT(_ textField: SKCustomTextField){
    
    let str : CharacterSet = CharacterSet(charactersIn: " ")
    if( textField.text!.trimmingCharacters(in: str) == "")
    {
        chkVal = "YES"
        textField.linesColor    = UIColor.red
        //textField.linesWidth = 2.0
    }else{
        textField.linesWidth = 1.0
        //textField.linesColor = AppCommonColor
    }
    
}

func SKCustomTextFieldDesign(_ textfield: SKCustomTextField, left:Bool ,right:Bool, top:Bool , bottom:Bool,lineColor:UIColor, lineWidth: CGFloat)
{
    textfield.leftLine      = left
    textfield.rightLine     = right
    textfield.topLine       = top
    textfield.bottomLine    = bottom
    textfield.linesColor    = lineColor
    textfield.linesWidth    = lineWidth
    
    textfield.drawLines()
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: textfield.frame.height))
    textfield.leftView = paddingView
    textfield.leftViewMode = UITextField.ViewMode.always
    
}

class FramedTextField: UITextField {
    
    @IBInspectable var linesWidth: CGFloat = 1.0 { didSet{ drawLines() } }
    
    @IBInspectable var linesColor: UIColor = UIColor.black { didSet{ drawLines() } }
    
    @IBInspectable var leftLine: Bool = false { didSet{ drawLines() } }
    @IBInspectable var rightLine: Bool = false { didSet{ drawLines() } }
    @IBInspectable var bottomLine: Bool = false { didSet{ drawLines() } }
    @IBInspectable var topLine: Bool = false { didSet{ drawLines() } }
    
    
    
    func drawLines() {
        
        if bottomLine {
            add(CGRect(x: 0.0, y: frame.size.height - linesWidth, width: frame.size.width, height: linesWidth))
        }
        
        if topLine {
            add(CGRect(x: 0.0, y: 0.0, width: frame.size.width, height: linesWidth))
        }
        
        if rightLine {
            add(CGRect(x: frame.size.width - linesWidth, y: frame.size.height - frame.size.height/4, width: linesWidth, height: frame.size.height/4))
        }
        
        if leftLine {
            add(CGRect(x: 0.0, y: frame.size.height - frame.size.height/4, width: linesWidth, height: frame.size.height/4))
        }
        
    }
    
    typealias Line = CGRect
    fileprivate func add(_ line: Line) {
        let border = CALayer()
        border.frame = line
        border.backgroundColor = linesColor.cgColor
        layer.addSublayer(border)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        drawLines()
    }
    
}



class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    func newBounds(_ bounds: CGRect) -> CGRect {
        
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= padding.top + padding.bottom
        newBounds.size.width -= padding.left + padding.right
        return newBounds
    }
}

extension UIImage {
    func resize(_ scale:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: size.width*scale, height: size.height*scale)))
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    func resizeToWidth(_ width:CGFloat)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}

func SKSquareImageTo(_ image: UIImage, size: CGSize) -> UIImage? {
    return SKResizeImage(SKSquareImage(image), targetSize: size)
}

func SKSquareImage(_ image: UIImage) -> UIImage? {
    let originalWidth  = image.size.width
    let originalHeight = image.size.height
    
    var edge: CGFloat
    if originalWidth > originalHeight {
        edge = originalHeight
    } else {
        edge = originalWidth
    }
    
    let posX = (originalWidth  - edge) / 2.0
    let posY = (originalHeight - edge) / 2.0
    
    let cropSquare = CGRect(x: posX, y: posY, width: edge, height: edge)
    
    let imageRef = (image.cgImage)?.cropping(to: cropSquare);
    return UIImage(cgImage: imageRef!, scale: UIScreen.main.scale, orientation: image.imageOrientation)
}

func SKResizeImage(_ image: UIImage?, targetSize: CGSize) -> UIImage? {
    if let image = image {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    } else {
        return nil
    }
}


class SKCustomFunction: NSObject {

}


@IBDesignable
class DesignableTableView: UITableView {
    
    @IBInspectable var backgroundImage: UIImage? {
        didSet {
            if let image = backgroundImage {
                let backgroundImage = UIImageView(image: image)
                backgroundImage.contentMode = UIView.ContentMode.scaleToFill
                backgroundImage.clipsToBounds = false
                self.backgroundView = backgroundImage
            }
        }
    }
    
}

/// Computed properties, based on the backing CALayer property, that are visible in Interface Builder.
@IBDesignable public class SKCustomView: UIView {
    /// When positive, the background of the layer will be drawn with rounded corners. Also effects the mask generated by the `masksToBounds' property. Defaults to zero. Animatable.
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }
        set {
            self.layer.cornerRadius = CGFloat(newValue)
            self.layer.masksToBounds = true
        }
    }
    
    /// The width of the layer's border, inset from the layer bounds. The border is composited above the layer's content and sublayers and includes the effects of the `cornerRadius' property. Defaults to zero. Animatable.
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    
    /// The color of the layer's border. Defaults to opaque black. Colors created from tiled patterns are supported. Animatable.
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor.init(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    /// The color of the shadow. Defaults to opaque black. Colors created from patterns are currently NOT supported. Animatable.
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor.init(cgColor: self.layer.shadowColor!)
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    /// The opacity of the shadow. Defaults to 0. Specifying a value outside the [0,1] range will give undefined results. Animatable.
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    /// The shadow offset. Defaults to (0, -3). Animatable.
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        }
        set {
            self.layer.shadowOffset = newValue
        }
    }
    
    /// The blur radius used to create the shadow. Defaults to 3. Animatable.
    @IBInspectable var shadowRadius: Double {
        get {
            return Double(self.layer.shadowRadius)
        }
        set {
            self.layer.shadowRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var shadowGradient: Bool {
        get {
            return false
        }
        set {
            addGradient()
        }
    }
    
    private var gradientLayer: CAGradientLayer?
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        addGradient()
    }
    
    func addGradient() {
        guard gradientLayer == nil else {
            gradientLayer?.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
            return
        }
        
        gradientLayer = CAGradientLayer()
        gradientLayer!.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        gradientLayer!.colors = [
            #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1).cgColor,
            #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1).cgColor
        ]
        gradientLayer!.locations = [0, 1]
        gradientLayer!.startPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer!.endPoint = CGPoint(x: 0.5, y: 1)
        self.layer.insertSublayer(gradientLayer!, at: 0)
    }
}
