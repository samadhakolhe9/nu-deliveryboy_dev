//
//  LocalDB.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 23/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import AwesomeCache
import SwiftyJSON

struct User {
    var customerUID: String! = ""
    var firstName: String! = ""
    var lastName: String! = ""
    var phoneNo: String! = ""
    var email: String! = ""
    var isValidated: String! = ""
}
class LocalDB: NSObject {
    
    static let shared = LocalDB()
    private override init() {}
    
    var UserInfo: User = User(customerUID: "", firstName: "", lastName: "", phoneNo: "", isValidated: "")
    
    func removeUserMobileNumber() -> String {
       do {
          let cacheUser = try Cache<NSString>(name: "UserInfo")
          cacheUser.removeAllObjects()
       } catch _ {
          print("Something went wrong :(")
       }
       return ""
    }
    
    func getUserMobileNumber() -> String {
       do {
          let cacheUser = try Cache<NSString>(name: "UserInfo")
           if cacheUser.object(forKey: "MobileNumber") != nil {
            return cacheUser.object(forKey: "MobileNumber")! as String
           }
       } catch _ {
          print("Something went wrong :(")
       }
       return ""
    }
    
    func setUserMobileNumber(phone: String) {
        globalUserPhone = phone
       do {
          let cacheUser = try Cache<NSString>(name: "UserInfo")
          cacheUser.setObject(phone as NSString, forKey: "MobileNumber")
       } catch _ {
          print("Something went wrong :(")
       }
    }
}
