
import UIKit


struct Messages {
    
    static let okayText = NSLocalizedString("OK", comment: "")
    static let dismissText = NSLocalizedString("Dismiss", comment: "")
    static let problemConnectingServer = NSLocalizedString("There was a problem connecting to server. Please try again", comment: "")
    static let internetMessage = NSLocalizedString("We are unable to update content. Please check your internet connection.", comment: "")
}

struct AppDetails {
    static let appName = "Nisarga Urja"
    static let appTitle = "Nisarga Urja"
}

struct colorCode {
    var backgroundColor:UIColor = .red
    var textColor:UIColor = .red
}

struct NavigationConstant {
    
}

public var iPhone5: Bool {
    return (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1136)
}
public var iPhone6_7_8: Bool {
    return (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1334)
}
public var iPhonePlus: Bool {
    return (UIDevice().userInterfaceIdiom == .phone && (UIScreen.main.nativeBounds.height == 1920 || UIScreen.main.nativeBounds.height == 2208))
}
public var iPhoneX: Bool {
    return (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436)
}
public var iPhoneXSMax: Bool {
    return (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2688)
}
public var iPhoneXR: Bool {
    return (UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1792)
}
public var alliPhoneX: Bool {
    return (UIDevice().userInterfaceIdiom == .phone && (UIScreen.main.nativeBounds.height == 2436 || UIScreen.main.nativeBounds.height == 2688 || UIScreen.main.nativeBounds.height == 1792))
}

func ChangeThemeWith(id: Int) {
    if id == 0 { // Light
        AppCommonColor          = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        ImageIconTintColor      = UIColor.white
        MenuButtonColor         = UIColor.white
        lightGrayColor          = UIColor.darkGray
        AppCommonBGColor        = UIColor.lightGray
    } else if id == 1 { // Dark
        AppCommonColor          = UIColor ( red: 0.101, green: 0.101, blue: 0.101, alpha: 1.0 )
        ImageIconTintColor      = UIColor.white
        MenuButtonColor         = UIColor.white
        lightGrayColor          = UIColor.lightGray
        AppCommonBGColor        = UIColor ( red: 0.224, green: 0.227, blue: 0.231, alpha: 1.0 )
    } else if id == 2 { // OLED
        AppCommonColor          = UIColor.black
        ImageIconTintColor      = UIColor.white
        MenuButtonColor         = UIColor.white
        lightGrayColor          = UIColor.lightGray
        AppCommonBGColor        = UIColor.black
    }
}
