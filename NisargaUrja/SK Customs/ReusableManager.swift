
import UIKit
import PopupDialog

class ReusableManager: NSObject {

    static let sharedInstance = ReusableManager()
    private override init() {}
    
    var popupDialog: PopupDialog = PopupDialog(title: "", message: "")
    var snackBar = Snackbar()
    
    func showAlert(alertString: String) {
//        if alertString == Messages.internetMessage {
//            showSnackBar(alertString: alertString)
//        } else {
            popupDialog = PopupDialog(title: AppDetails.appTitle, message: alertString)
            popupDialog.transitionStyle = .fadeIn
            let okayButton = CancelButton(title: Messages.okayText) {
                
            }
            okayButton.titleColor = .red
            popupDialog.addButton(okayButton)
            UIApplication.topViewController()?.present(popupDialog, animated: true, completion: {
                
            })
//        }
    }

    func showSuccessAlert(alertString: String) {
        popupDialog = PopupDialog(title: AppDetails.appTitle, message: alertString)
        popupDialog.transitionStyle = .fadeIn
        let okayButton = CancelButton(title: Messages.okayText) {
            
        }
        okayButton.titleColor = .gray
        popupDialog.addButton(okayButton)
        UIApplication.topViewController()?.present(popupDialog, animated: true, completion: {
            
        })
    }
    
    func showSnackBar(alertString: String) {
        snackBar.dismiss()
        snackBar = Snackbar.init(message: alertString, duration: .forever, actionText: Messages.dismissText) { (snackbar) in
            snackbar.dismiss()
        }
        snackBar.animationType = .slideFromBottomBackToBottom
        snackBar.shouldDismissOnSwipe = true
        snackBar.leftMargin = 0
        snackBar.rightMargin = 0
        snackBar.topMargin = 20
        snackBar.cornerRadius = 0
        snackBar.duration = .short
        snackBar.backgroundColor = .black
        snackBar.messageTextColor = .white
        snackBar.messageTextFont = UIFont.systemFont(ofSize: 12)
        snackBar.show()
    }
    
}
