//
//  CustomNavigationBar.swift
//  NisargaUrja
//
//  Created by Bipin Tamkhane on 02/09/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
    func transparentNavigationBar() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
        self.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.tintColor = .white
    }
    
    func nonTransparentNavigationBar() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
        self.titleTextAttributes = [.foregroundColor: UIColor.black]
        self.tintColor = .black
        self.backgroundColor = .white
    }
    
    func setNavbar(backgroundColorAlpha alpha: CGFloat, newColor: UIColor) {
        if #available(iOS 13, *) {
            self.standardAppearance.backgroundColor = newColor.withAlphaComponent(alpha)
            self.standardAppearance.backgroundEffect = nil
            self.standardAppearance.shadowImage = UIImage()
            self.standardAppearance.shadowColor = .clear
            self.standardAppearance.backgroundImage = UIImage()
        } else {
            self.setBackgroundImage(UIImage(), for: .default)
            self.shadowImage = UIImage()
            self.backgroundColor = newColor.withAlphaComponent(alpha)
        }
    }
}
