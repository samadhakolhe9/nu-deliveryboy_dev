

import UIKit

struct APIConstants {
    
    static let base_url = "http://34.69.81.97:3070/api/v1"
    
//Login
    static let generateOTP            =  base_url + "/delivery-generate-otp?"
    static let validateOTP            =  base_url + "/delivery-validate-otp?"
    
    
    
    
    
    
    
    static let getDeliveryPincodes    =  base_url + "/get-delivery-pincodes"
    static let registerCustomer       =  base_url + "/register-customer?"
    static let setAPNToken            =  base_url + "/set-token?"
    
//Home
    static let getDefaultAddress      =  base_url + "/default-address?"
    static let getBanners             =  base_url + "/banners"
    static let getUpcomingOrders      =  base_url + "/upcoming-orders?"
    static let getFavouriteProducts   =  base_url + "/favourite-products?"
    static let getCategories          =  base_url + "/categories"
    static let getCoupons             =  base_url + "/coupons?"
    static let getFavouriteRecipes    =  base_url + "/favourite-recipes?"
    static let getBrands              =  base_url + "/brands"
    static let getBestsellers         =  base_url + "/bestsellers?"
    static let getTestimonials        =  base_url + "/testimonials"
    
//Customer
    static let getUserInfo            =  base_url + "/customer?"
    
//Brand
    static let getBrandProducts       =  base_url + "/brand-products?"
    
    
//Product
    static let getProductDetails      =  base_url + "/product-details?"
    static let setFavorites           =  base_url + "/toggle-favourite-status?"
    static let getProductTags         =  base_url + "/product-tags?"  //MISSING  //Not required its already return in product details page
    static let getRelatedRecipes      =  base_url + "/related-recipes?"
    
    
//Category
    static let getCategoryProducts    =  base_url + "/category-products?"
    
    
//Search
    static let getSearchResult        =  base_url + "/search?"
    
    
//Order
    static let getAllOrder            =  base_url + "/orders?"
    static let getOrderDetails        =  base_url + "/order-detail?"
    static let placeOrder             =  base_url + "/place-order?"  //MISSING
    
    
//Address
    static let getAddresses           =  base_url + "/addresses?"
    static let addAddress             =  base_url + "/add-address?"
    static let editAddress            =  base_url + "/edit-address?"
    static let setasdefaultAddress    =  base_url + "/setasdefault-address?"
    static let setasBillingAddress    =  base_url + "/setasbilling-address?"
    static let deleteAddress          =  base_url + "/delete-address?"
    
    
//Recipe
    static let getRecipeDetails       =  base_url + "/recipe-details?"
    static let toggleFavouriteRecipe  =  base_url + "/toggle-favourite-recipe?"
    
    
//Auther
    static let getAuthorDetails       =  base_url + "/author-details?"
    
    
//Carts
    static let addToCartItem          =  base_url + "/add-cart-item?"
    static let removeFromCartItem     =  base_url + "/remove-cart-item?"
    static let getcartItem            =  base_url + "/cart-items?"
    static let getcartTotals          =  base_url + "/cart-totals?"
    static let applyCredits           =  base_url + "/apply-credits?"
    static let removeCredits          =  base_url + "/remove-credits?"
    static let cartOffers             =  base_url + "/cart-offers?"
    static let applyOffer             =  base_url + "/apply-offer?"
    static let removeOffer            =  base_url + "/remove-offer?"
    static let applyLoyaltyPoints     =  base_url + "/apply-loyalty-points?"
    static let removeLoyaltyPoints    =  base_url + "/remove-loyalty-points?"
    static let totalLoyaltyPoints     =  base_url + "/total-loyalty-points?"
    static let totalCreditPoints      =  base_url + "/total-credits?"
    
    
//Credits
    static let getCreditsEarned       =  base_url + "/credits-earned?" // Not in requirement
    static let getCreditsUsed         =  base_url + "/credits-used?"   // Not in requirement
    
    
//Loyalty
    static let getLoyaltyPointsEarned =  base_url + "/loyalty-points-earned?" // Not in requirement
    static let getLoyaltyPointsUsed   =  base_url + "/loyalty-points-used?"   // Not in requirement
    
    
//Offers
    static let getOffers              =  base_url + "/get-offer?"
    
//Coupon
    static let applyCoupon            =  base_url + "/apply-coupon?"
    static let removeCoupon           =  base_url + "/remove-coupon?"
    static let getAppliedCoupon       =  base_url + "/applied-coupon?"
    
    
//Delivery
    static let getDeliveryRates       =  base_url + "/delivery-rates?"  //MISSING //No need to call , delivery rates return in cart total
    

//Delivery Boy
    static let deliveryOrders         =  base_url + "/delivery-orders?"
    static let deliveryOrderDetail   =  base_url + "/delivery-order-detail?"
    
}

