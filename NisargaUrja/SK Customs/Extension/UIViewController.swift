//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!.imageWithColor(color: .black))
 //  self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!.imageWithColor(color: .black))
        self.addRightButtonWithImage(UIImage(named: "search_icon")!.imageWithColor(color: .black))
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    public func addLeftButtonWithImage(_ buttonImage: UIImage) {
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.toggleLeftButton))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    public func addRightButtonWithImage(_ buttonImage: UIImage) {
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: buttonImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.toggleRightButton))
        navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc public func toggleLeftButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc public func toggleRightButton() {
        
        //self.navigationController?.popViewController(animated: true)
    }
}
