
import UIKit

class SK_Extensions: NSObject {

}

let deviceWidth = UIScreen.main.bounds.size.width

extension String
{
    func trim() -> String {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
}

extension UITextField {
    @IBInspectable var inset: CGFloat{
        get {
            
            return 0
        }
        set (size) {
            self.textRect(forBounds: bounds.insetBy(dx: inset, dy: inset))
        }
    }
    
    @IBInspectable var skTextSize: Bool {
        get {
            
            return false
        }
        set (size) {
            if(size)
            {
            self.font = self.font!.withSize((self.font?.pointSize)!+deviceWidth/200)
            }
        }
    }
    @IBInspectable var CommonTextColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.textColor = AppCommonColor
            }
        }
    }
    @IBInspectable var commonBackColor: Bool {
        get {
            
            return false
        }
        set (backColor) {
            if(backColor)
            {
                self.backgroundColor = AppCommonColor
            }
        }
    }
    
    @IBInspectable var isPhoneNumber: Bool {
        get {
            
            return false
        }
        set (isNo) {
            if(isNo)
            {
                self.keyboardType = UIKeyboardType.decimalPad
                let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
                doneToolbar.barStyle = UIBarStyle.default
                
                let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
                let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.dismissKeyBoard))
                doneToolbar.items = [flexSpace,done]
                doneToolbar.sizeToFit()
                
                self.inputAccessoryView = doneToolbar
            }
        }
    }
    
    @objc func dismissKeyBoard()
    {
        self.resignFirstResponder()
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.dismissKeyBoard))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    

}
extension Array {
    func split() -> (left: [Element], right: [Element]) {
        let ct = self.count
        let half = ct / 2
        let leftSplit = self[0 ..< half]
        let rightSplit = self[half ..< ct]
        return (left: Array(leftSplit), right: Array(rightSplit))
    }
}
extension UILabel {
    
    @IBInspectable
    var letterSpace: CGFloat {
        set {
            let attributedString: NSMutableAttributedString!
            if let currentAttrString = attributedText {
                attributedString = NSMutableAttributedString(attributedString: currentAttrString)
            }
            else {
                attributedString = NSMutableAttributedString(string: text ?? "")
                text = nil
            }

            attributedString.addAttribute(NSAttributedString.Key.kern,
                                           value: newValue,
                                           range: NSRange(location: 0, length: attributedString.length))

            attributedText = attributedString
        }

        get {
            if let currentLetterSpace = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: .none) as? CGFloat {
                return currentLetterSpace
            }
            else {
                return 0
            }
        }
    }
    
//    @IBInspectable var skTextSpace: Double {
//        get {
//
//            return 0.0
//        }
//        set (kernValue) {
//           if let labelText = text, labelText.count > 0 {
//              let attributedString = NSMutableAttributedString(string: labelText)
//            attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
//              attributedText = attributedString
//            }
//        }
//    }
    
    @IBInspectable var skTextSize: Bool {
        get {
            
            return false
        }
        set (size) {
            if(size)
            {
            self.font = self.font!.withSize((self.font?.pointSize)!+deviceWidth/200)
            }
        }
    }
    @IBInspectable var CommonTextColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.textColor = AppCommonColor
            }
        }
    }
    @IBInspectable var commonBackColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.backgroundColor = AppCommonColor
            }
        }
    }
}

extension UITextView {
    @IBInspectable var skTextSize: Bool {
        get {
            
            return false
        }
        set (size) {
            if(size)
            {
            self.font = self.font!.withSize((self.font?.pointSize)!+deviceWidth/200)
            }
        }
    }
    @IBInspectable var CommonTextColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.textColor = AppCommonColor
            }
        }
    }
    @IBInspectable var commonBackColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.backgroundColor = AppCommonColor
            }
        }
    }
    
    @objc func dismissKeyBoard()
    {
        self.resignFirstResponder()
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.dismissKeyBoard))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
}

extension UIButton {
    @IBInspectable var skTextSize: Bool {
        get {
            
            return false
        }
        set (size) {
            if(size)
            {
            self.titleLabel!.font = self.titleLabel!.font.withSize((self.titleLabel!.font.pointSize)+deviceWidth/200)
            }
        }
    }
    @IBInspectable var CommonTextColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.titleLabel?.textColor = AppCommonColor
            }
        }
    }
    @IBInspectable var commonBackColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.backgroundColor = AppCommonColor
            }
        }
    }
}

extension UIImage {
    func imageWithColor(color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

public extension UINavigationController {
    
    /**
     Pop current view controller to previous view controller.
     
     - parameter type:     transition animation type.
     - parameter duration: transition animation duration.
     */
    func pop(transitionType type: String = CATransitionType.fade.rawValue, duration: CFTimeInterval = 0.3) {
        self.addTransition(transitionType: type, duration: duration)
        self.popViewController(animated: false)
    }
    
    /**
     Push a new view controller on the view controllers's stack.
     
     - parameter vc:       view controller to push.
     - parameter type:     transition animation type.
     - parameter duration: transition animation duration.
     */
    func push(viewController vc: UIViewController, transitionType type: String = CATransitionType.fade.rawValue, duration: CFTimeInterval = 0.3) {
        self.addTransition(transitionType: type, duration: duration)
        self.pushViewController(vc, animated: false)
    }
    
    private func addTransition(transitionType type: String = CATransitionType.fade.rawValue, duration: CFTimeInterval = 0.3) {
        let transition = CATransition()
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType(rawValue: type)
        self.view.layer.add(transition, forKey: nil)
    }
    
}

extension UIView
{
    @IBInspectable var skViewBorder: Bool {
        get {
            
            return false
        }
        set (flag) {
            self.layer.borderColor = UIColor.white.cgColor
            self.layer.borderWidth = 0.2
            self.layer.cornerRadius = 2
            self.clipsToBounds = true
            self.layer.masksToBounds = false
            
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowOpacity = 0.5
            self.layer.shadowRadius = 0.2
        }
        
    }
    
    @IBInspectable var skRoundBorder: Bool {
        get {
            
            return false
        }
        set (flag) {
            self.layer.cornerRadius = self.frame.size.height/2
        }
        
    }
    
    @IBInspectable var CommonBackgroundColor: Bool {
        get {
            
            return false
        }
        set (color) {
            if(color)
            {
                self.backgroundColor = AppCommonColor
            }
        }
    }
    
    @IBInspectable var skShadow: Bool {
        get {
            
            return false
        }
        set (flag) {
            self.layer.borderColor = UIColor.gray.cgColor
            self.layer.borderWidth = 0.2
            self.layer.cornerRadius = 2
            
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowOpacity = 0.5
            self.layer.shadowRadius = 5
        }
        
    }
    
}



