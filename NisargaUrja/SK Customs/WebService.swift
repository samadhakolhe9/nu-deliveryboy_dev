import Foundation
import UIKit
import Alamofire
import RappleProgressHUD
import SwiftyJSON

struct Response {
    var keys:JSON
    var length:Int
    var fields:JSON
    var fieldLookup:JSON
}

class ApiManager : NSObject {
    
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
     //MARK:- HEADER Parameter
     func headers() -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Access-Control-Allow-Origin" : "*"
        ]
        headers["Authorization"] = "d7+#s^nJkfB$^c6fmJ8&A7?P"

        return headers
    }
    
    var APIResponseData:[Response] = []
    
     //MARK:- POST Method
    func postApiRequest(url:String, dict: [String : Any], runLoader: Bool, showError : Bool, completion:@escaping (_ response:[Response], _ success: Bool) -> Void) {
        var urlString = url
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        debugPrint("Request POST URL: %@ PARAM: %@", urlString, dict)

        if runLoader {
            RappleActivityIndicatorView.startAnimatingWithLabel("", attributes: RappleModernAttributes)
        }
        AF.request(urlString, method: .post, parameters: dict, encoding: JSONEncoding.default, headers: headers()) .responseJSON { responseObject in
                  switch(responseObject.result) {
                  case .success(_):
                      if responseObject.value != nil {
                          if runLoader {
                               //Progress.instance.hide()
                              DispatchQueue.main.async {
                                  RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: "", completionTimeout: 0.1)
                              }
                          }
                          debugPrint(responseObject.value as Any)
                          let statusCode = responseObject.response?.statusCode
                          let result = JSON(responseObject.value!)
                          self.APIResponseData = []
                          if statusCode == 200 {
                            if result.count > 0 {
                                for data in result {
                                    self.APIResponseData.append(Response(keys: data.1["keys"], length: data.1["length"].intValue, fields: data.1["_fields"], fieldLookup: data.1["_fieldLookup"]))
                                }
                                completion(self.APIResponseData, true)
                            } else {
                                completion(self.APIResponseData, result.stringValue == "SUCCESS" ? true : false)
                            }
                          }else if statusCode == 204 && showError {
                              self.errorHandling(errorCode: statusCode!, errorMessage: "No Result Found", JSON: [:])
                          }else {
                            if result["error"].stringValue != "" && showError {
                                  self.errorHandling(errorCode: statusCode!, errorMessage: result["error"].stringValue, JSON: result)
                            }else if result["message"].stringValue != "" && showError {
                                  self.errorHandling(errorCode: statusCode!, errorMessage: result["message"].stringValue, JSON: result)
                              }
                          }
                      } else if showError {
                          ReusableManager.sharedInstance.showAlert(alertString: responseObject.error!.localizedDescription)
                      }
                      break
                  case .failure(let error):
                        if runLoader {
                            DispatchQueue.main.async {
                                RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: "", completionTimeout: 0.1)
                            }
                        }
                        if showError {
                            ReusableManager.sharedInstance.showAlert(alertString: error.localizedDescription)
                        }
                      break
                  }
              }
    }
    

    
     //MARK:- GET Method
    func getRequestApi(url:String, runLoader : Bool, showError : Bool, completion:@escaping (_ response:[Response], _ success: Bool) -> Void) {
        var urlString = url
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        debugPrint("Request GET URL:\(urlString)")
        if runLoader {
           RappleActivityIndicatorView.startAnimatingWithLabel("Processing...", attributes: RappleModernAttributes)
        }
        AF.request(urlString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers()) .responseJSON { responseObject in
            switch(responseObject.result) {
            case .success(_):
                if responseObject.value != nil {
                    if runLoader {
                         //Progress.instance.hide()
                        DispatchQueue.main.async {
                            RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: "", completionTimeout: 0.1)
                        }
                    }
                    debugPrint(responseObject.value as Any)
                    let statusCode = responseObject.response?.statusCode
                    let result = JSON(responseObject.value!)
                    if statusCode == 200 {
                        if result.count > 0 {
                            for data in result {
                                self.APIResponseData.append(Response(keys: data.1["keys"], length: data.1["length"].intValue, fields: data.1["_fields"], fieldLookup: data.1["_fieldLookup"]))
                            }
                            completion(self.APIResponseData, true)
                        } else {
                            completion(self.APIResponseData, result.stringValue == "SUCCESS" ? true : false)
                        }
                    }else if statusCode == 204 && showError {
                        self.errorHandling(errorCode: statusCode!, errorMessage: "No Result Found", JSON: [:])
                    }else {
                      if result["error"].stringValue != "" && showError {
                            self.errorHandling(errorCode: statusCode!, errorMessage: result["error"].stringValue, JSON: result)
                      }else if result["message"].stringValue != "" && showError {
                            self.errorHandling(errorCode: statusCode!, errorMessage: result["message"].stringValue, JSON: result)
                        }
                    }
                } else if showError {
                    ReusableManager.sharedInstance.showAlert(alertString: responseObject.error!.localizedDescription)
                } else {
                    completion(self.APIResponseData, true)
                }
                break
            case .failure(let error):
                if runLoader {
                    DispatchQueue.main.async {
                        RappleActivityIndicatorView.stopAnimation(completionIndicator: .none, completionLabel: "", completionTimeout: 0.1)
                    }
                }
                if showError {
                    ReusableManager.sharedInstance.showAlert(alertString: error.localizedDescription)
                }

                break
            }
        }
    }
    


    
    //MARK:- ERROR Handling Method
    func errorHandling(errorCode: Int, errorMessage:String,JSON:JSON) {
        if errorCode == 401 {
            ReusableManager.sharedInstance.showAlert(alertString: errorMessage)
        } else if errorCode == 426{
            ReusableManager.sharedInstance.showAlert(alertString: errorMessage)
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: errorMessage)
        }
    }
    
    
    static var components: URLComponents {
        var components = URLComponents()
        components.scheme = "http"
        components.host = "34.69.81.97"
        components.port = 3070
        return components
    }
}

/*//MARK:- Loader Class
class LoaderClass {
    class func startLoader() {
        let activityData = ActivityData(size: nil, message: nil, messageFont: nil, type: NVActivityIndicatorType.ballRotateChase, color: .white, padding: 0, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR, textColor: nil)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    class func stopLoader() {
          NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}*/

