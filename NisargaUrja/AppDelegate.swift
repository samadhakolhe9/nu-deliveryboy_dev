//
//  AppDelegate.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 01/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import UserNotifications

var globalNotoficationCount = "0"
var userDeviceToken = ""
var isFromNotificationCenter = false
var isFromAppDelegate = false
var isOnNotificationPage = false

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
       GMSPlacesClient.provideAPIKey("AIzaSyBbDlxJEJp3cyz3VLkzyLxME5jgSxgFgpQ")
       GMSServices.provideAPIKey("AIzaSyBbDlxJEJp3cyz3VLkzyLxME5jgSxgFgpQ")
        
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        globalNotoficationCount = "\(UIApplication.shared.applicationIconBadgeNumber)"
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in

            // If granted comes true you can enabled features based on authorization.
            guard granted else { return }
            DispatchQueue.main.async(execute: {
              application.registerForRemoteNotifications()
            })
        }
        
        UNUserNotificationCenter.current().getNotificationSettings(){ (setttings) in
            switch setttings.soundSetting{
            case .enabled:
                print("enabled sound")
            case .disabled:
                print("not allowed notifications")
            case .notSupported:
                print("something went wrong here")
            @unknown default:
                fatalError()
            }
        }
        return true
    }
    
    var myOrientation: UIInterfaceOrientationMask = .portrait
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification
        userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let state = application.applicationState
        switch state {
            
        case .inactive:
            updateNotificationCount(count: 1)
        case .background:
            updateNotificationCount(count: 1)
        case .active:
            updateNotificationCount(count: 1)
        @unknown default:
            updateNotificationCount(count: 1)
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    private func application(application: UIApplication,
                             didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        //com.Intelligenttag.Symmetry.Host.MLCV-II.1 // com.push.appleapnsitttest
        print("Registered Notification")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print(error.localizedDescription)
        print("Not registered notification")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        
        userDeviceToken = token
        print(deviceToken.description)
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
        }
        UserDefaults.standard.setValue(token, forKey: "ApplicationIdentifier")
        UserDefaults.standard.synchronize()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Notification response : ", response)
        isFromNotificationCenter = false
        if globalUserPhone != "" && !isOnNotificationPage {
            isFromAppDelegate = true
            self.window!.rootViewController?.dismiss(animated: false, completion: nil)
            
            let rootViewController = self.window!.rootViewController as! UINavigationController
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "idMyOrdersViewController") as! MyOrdersViewController
            rootViewController.pushViewController(viewController, animated: true)
        } else {
            isFromNotificationCenter = true
            NotificationCenter.default.post(name: Notification.Name("didReceivedNotification"), object: nil, userInfo: nil)
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}

func updateNotificationCount(count: Int) {
    if count == 0 {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    globalNotoficationCount = "\(UIApplication.shared.applicationIconBadgeNumber)"
    NotificationCenter.default.post(name: Notification.Name("didReceiveNotification"), object: nil, userInfo: ["count": globalNotoficationCount])
}
    
