//
//  ViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 01/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
var globalUserPhone = ""

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            globalUserPhone = "8446628681" //LocalDB.shared.getUserMobileNumber() //
            if globalUserPhone != "" {
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "idMyOrdersViewController") as! MyOrdersViewController
                self.navigationController?.push(viewController: viewController)
            } else {
                let loginCV = self.storyboard?.instantiateViewController(withIdentifier: "idLoginViewController") as! LoginViewController
                self.navigationController?.push(viewController: loginCV)
            }
        }
    }


}

