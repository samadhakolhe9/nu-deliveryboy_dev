//
//  MyOrdersTableViewCell.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 13/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class MyOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var leftIndicationBar: UIView!
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblAddressHeader: UILabel!
    @IBOutlet weak var lblCustomerMobile: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var lblDeliveryStatus: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblPaymentDeliveryStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        outerView.applyCommonShadow()
        lblPaymentDeliveryStatus.layer.borderWidth = 0.5
        lblPaymentDeliveryStatus.layer.cornerRadius = 5
        lblPaymentDeliveryStatus.layer.masksToBounds = false
        lblPaymentDeliveryStatus.clipsToBounds = true
        
        lblDeliveryStatus.layer.borderWidth = 0.5
        lblDeliveryStatus.layer.cornerRadius = 5
        lblDeliveryStatus.layer.masksToBounds = false
        lblDeliveryStatus.clipsToBounds = true
        
        leftIndicationBar.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
