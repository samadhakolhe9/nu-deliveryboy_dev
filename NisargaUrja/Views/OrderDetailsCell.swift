//
//  OrderDetailsCell.swift
//  NisargaUrja
//
//  Created by Bipin Tamkhane on 04/09/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class OrderDetailsCell: UITableViewCell {

    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductQty: UILabel!
    @IBOutlet weak var lblProductRate: UILabel!
    @IBOutlet weak var lblProductTotalPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
