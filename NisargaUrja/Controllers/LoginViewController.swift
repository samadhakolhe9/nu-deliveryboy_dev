//
//  LoginViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 01/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imgAppIcon: UIImageView!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var btnSMS: UIButton!
    @IBOutlet weak var btnWhatsapp: UIButton!
    @IBOutlet weak var btnBrowse: UIButton!
    @IBOutlet weak var containtView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgAppIcon.alpha = 0.0
        self.containtView.alpha = 0.0
        UIView.animate(withDuration: 3.0) {
            self.imgAppIcon.alpha   = 1.0 
            self.containtView.alpha = 1.0
        }
        
        btnSMS.applyCommonShadow(shadowColor: .gray, backgroundColor: UIColor.white, radius: btnSMS.bounds.height/2)
        btnWhatsapp.applyCommonShadow(shadowColor: .gray, backgroundColor: UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1), radius: btnWhatsapp.bounds.height/2)
            
        
        btnBrowse.titleLabel!.textAlignment = .right
        btnBrowse.titleLabel!.attributedText = NSMutableAttributedString(string: "BROWSE >", attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.kern: 2.7])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func SMSButtonAction(_ sender: UIButton) {
        if txtMobileNumber.text != "" {
            let urlString = APIConstants.generateOTP+"phoneNo=\(txtMobileNumber.text!)&otpmode=sms"
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
                if success {
                    let KeysArray = Response[0].fieldLookup
                    let fieldsArray = Response[0].fields
                    if fieldsArray[KeysArray["isValidated"].intValue].boolValue {
                       // LocalDB.shared.setUserInfoToLocal(user: Response[0].fields)
                    }
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "idVerifyOTPController") as! VerifyOTPController
                    controller.isSMS = false
                    controller.mobileNumber = self.txtMobileNumber.text!
                    self.navigationController?.pushViewController(controller, animated: true)
                } else {
                    ReusableManager.sharedInstance.showAlert(alertString: "Failed, Please try again.")
                }
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: "First enter your mobile number to send OTP.")
        }
    }

    @IBAction func WhatsappButtonAction(_ sender: UIButton) {
        if txtMobileNumber.text != "" {
            let urlString = APIConstants.generateOTP+"phoneNo=\(txtMobileNumber.text!)&otpmode=whatsapp"
            ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
                if success {
                    let KeysArray = Response[0].fieldLookup
                    let fieldsArray = Response[0].fields
                    if fieldsArray[KeysArray["isValidated"].intValue].boolValue {
                       // LocalDB.shared.setUserInfoToLocal(user: Response[0].fields)
                    }
                    let controller = self.storyboard?.instantiateViewController(withIdentifier: "idVerifyOTPController") as! VerifyOTPController
                    controller.isSMS = false
                    controller.mobileNumber = self.txtMobileNumber.text!
                    self.navigationController?.pushViewController(controller, animated: true)
                } else {
                    ReusableManager.sharedInstance.showAlert(alertString: "Failed, Please try again.")
                }
            }
        } else {
            ReusableManager.sharedInstance.showAlert(alertString: "First enter your mobile number to send OTP.")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
