//
//  OrderDetailViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 17/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import SwiftyJSON
import MessageUI

class InvoiceViewController: UITableViewController {

    @IBOutlet weak var lblUsernName: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var lblOrderDateValue: UILabel!
    @IBOutlet weak var lblDeliveryDateValue: UILabel!
    @IBOutlet weak var lblPaid: UILabel!
    @IBOutlet weak var lblPaidWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDelivered: UILabel!
    @IBOutlet weak var lblDeliveredWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNameHeading: UILabel!
    @IBOutlet weak var lblQtyHeading: UILabel!
    @IBOutlet weak var lblRateHeading: UILabel!
    @IBOutlet weak var lblTotalHeading: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblSubtotalValue: UILabel!
    @IBOutlet weak var lblLoyaltyPoints: UILabel!
    @IBOutlet weak var lblCreditsUsed: UILabel!
    @IBOutlet weak var lblCouponCode: UILabel!
    @IBOutlet weak var lblTotalDiscount: UILabel!
    @IBOutlet weak var lblDeliveryCharges: UILabel!
    @IBOutlet weak var lblLoyaltyPointsValue: UILabel!
    @IBOutlet weak var lblCreditsUsedValue: UILabel!
    @IBOutlet weak var lblCouponCodeValue: UILabel!
    @IBOutlet weak var lblTotalDiscountValue: UILabel!
    @IBOutlet weak var lblDeliveryChargesValue: UILabel!
    @IBOutlet weak var lblUsedLoyaltyPoints: UILabel!
    @IBOutlet weak var lblAppliedCouponCode: UILabel!
    @IBOutlet weak var lblAppliedTotalDiscount: UILabel!
    @IBOutlet weak var lblFreeDeliveryCharges: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalValue: UILabel!
    @IBOutlet weak var lblBillPaymentStatus: UILabel!
    @IBOutlet weak var lblBillPaymentStatusWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblFloorNumber: UILabel!
    @IBOutlet weak var lblShippingAddress: UILabel!
    @IBOutlet weak var btnGoToMap: UIButton!
    @IBOutlet weak var lblDeliveryNote: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var txtPaymentModeDropdown: DropDown!
    @IBOutlet weak var viewForFooter: UIView!
    @IBOutlet weak var btnMarkAsPaid: UIButton!
    @IBOutlet weak var btnMarkAsDelivered: UIButton!
    

    var orderDetails: [OrderData] = []
    var selectedOrderUID = ""
    struct OrderData {
        var firstName: String?
        var lastName: String?
        var phoneNo: String?
        var note: String?
        var totalDiscount: Int?
        var orderUID: String?
        var orderSubtotal: String?
        var orderTotal: String?
        var orderDate: String?
        var deliveryDate: String?
        var orderStatus: String?
        var deliveryStatus: String?
        var shippingAddress: String?
        var house: String?
        var deliveryCharges: String?
        var productName: [String]?
        var quantityPurchased: [String]?
        var variationTitle: [String]?
        var discountPrice: [String]?
        var couponUsed: String?
        var couponDiscount: String?
        var loyaltyPointsUsed: String?
        var creditsUsed: String?
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "OrderDetailsCell", bundle: nil), forCellReuseIdentifier: "OrderDetailsCell")
        self.tableView.estimatedRowHeight = 40
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.tableFooterView = viewForFooter
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        setupUI()
        getOrderDetails()
    }
        
    override func viewDidAppear(_ animated: Bool) {
        let paymentModeArray = ["CASH", "ONLINE"]
        txtPaymentModeDropdown.text = paymentModeArray.count > 0 ? paymentModeArray[0] : ""
        txtPaymentModeDropdown.isSearchEnable = false
        txtPaymentModeDropdown.hideOptionsWhenSelect = true
        txtPaymentModeDropdown.checkMarkEnabled = false
        txtPaymentModeDropdown.optionArray = paymentModeArray
        txtPaymentModeDropdown.didSelect{(selectedTextArray , indexs , ids) in }
    }
    
    func setupUI() {
        lblPaid.layer.borderWidth = 0.5
        lblPaid.layer.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        lblPaid.layer.cornerRadius = 5
        lblPaid.clipsToBounds = true
        lblDelivered.layer.borderWidth = 0.5
        lblDelivered.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        lblDelivered.layer.cornerRadius = 5
        lblDelivered.clipsToBounds = true
        
        btnMarkAsPaid.layer.cornerRadius = btnMarkAsPaid.layer.frame.size.height / 2
        btnMarkAsPaid.clipsToBounds = true
        btnMarkAsDelivered.layer.cornerRadius = btnMarkAsDelivered.layer.frame.size.height / 2
        btnMarkAsDelivered.clipsToBounds = true
    }
    
    @IBAction func btnCallPressed(_ sender: UIButton) {
    }
    
    @IBAction func btnGoToMapPressed(_ sender: UIButton) {
    }
    
    @IBAction func btnMarkAsPaidPressed(_ sender: UIButton) {
    }
    
    @IBAction func btnMarkAsDeliveredPressed(_ sender: UIButton) {
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.orderDetails.count == 0 {
            return 0
        }

        return section == 3 ? self.orderDetails[0].productName!.count+1 : super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 3 && indexPath.row > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailsCell", for: indexPath) as! OrderDetailsCell
            let newIndex = indexPath.row-1

            cell.lblProductName.text = orderDetails[0].productName![newIndex] + " - " + orderDetails[0].variationTitle![newIndex]
            cell.lblProductQty.text = orderDetails[0].quantityPurchased![newIndex]
            cell.lblProductRate.text = "₹ " + orderDetails[0].discountPrice![newIndex]

            let discountPrice = Double("\(orderDetails[0].discountPrice![newIndex])")!
            let quantityPurchased = Double("\(orderDetails[0].quantityPurchased![newIndex])")!

            cell.lblProductTotalPrice.text = "₹ " + String(discountPrice * quantityPurchased)
            
            return cell
        }
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
           if indexPath.section == 3 && indexPath.row > 0 {
               let newIndexPath = IndexPath(row: 0, section: indexPath.section)
               return super.tableView(tableView, indentationLevelForRowAt: newIndexPath)
           }
           return super.tableView(tableView, indentationLevelForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 3 && indexPath.row > 0 ? UITableView.automaticDimension : super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return section > 4 ?  60 : 0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return section > 4 ? viewForFooter : UIView()
    }
    
    
    func updateUI() {
        self.orderDetails.forEach { (detail) in
            lblUsernName.text                      = detail.firstName! + " " + detail.lastName!
            lblFloorNumber.text                    = detail.house
            lblShippingAddress.text                = detail.shippingAddress
            lblMobileNumber.text                   = detail.phoneNo
            lblDeliveryNote.text                   = detail.note != "" ? detail.note : "No Additional Notes"
            lblTotalDiscountValue.text             = String(detail.totalDiscount!)
            
            let oDateString                     = detail.orderDate!
            let orderDate                       = oDateString.toDateString(inputDateFormat: "dd-mm-yyyy", ouputDateFormat: "MMM d, yyyy")
            let dDateString                     = detail.orderDate!
            let deliveryDate                    = dDateString.toDateString(inputDateFormat: "dd-mm-yyyy", ouputDateFormat: "MMM d, yyyy")
            lblOrderDateValue.text              = orderDate
            lblDeliveryDateValue.text           = deliveryDate
            lblSubtotalValue.text               = "₹ " + String(describing: detail.orderSubtotal!)
            lblTotalValue.text                  = "₹ " + String(describing: detail.orderTotal!)

            lblPaid.text                        = String(describing: detail.orderStatus!)
            lblDelivered.text                   = String(describing: detail.deliveryStatus!)
            
            if String(describing: detail.deliveryStatus!) == "NOT DELIVERED" {
                lblDeliveredWidthConstraint.constant = 85
            } else {
                lblDeliveredWidthConstraint.constant = 65
            }
            
            setStatusBorder(label: lblPaid)
            setStatusBorder(label: lblDelivered)
            let loyaltyPointsUsed   = Double("\(orderDetails[0].loyaltyPointsUsed != "" ? orderDetails[0].loyaltyPointsUsed! : "0")")!
            let creditsUsed         = Double("\(orderDetails[0].creditsUsed != "" ? orderDetails[0].creditsUsed! : "0")")!
            let couponDiscount      = Double("\(orderDetails[0].couponDiscount != "" ? orderDetails[0].couponDiscount! : "0")")!

            lblUsedLoyaltyPoints.text           = "0 points used"
            lblLoyaltyPointsValue.text          = "₹ " + String(describing: detail.loyaltyPointsUsed != "" ? detail.loyaltyPointsUsed! : "0")
            lblCreditsUsedValue.text            = "₹ " + String(describing: detail.creditsUsed != "" ? detail.creditsUsed! : "0")
            lblCouponCodeValue.text             = "₹ " + String(describing: detail.couponDiscount != "" ? detail.couponDiscount! : "0")
            lblAppliedCouponCode.text           = String(describing: detail.couponUsed!)
            lblTotalDiscountValue.text          = "₹ " + String(describing: loyaltyPointsUsed+creditsUsed+couponDiscount)
            lblDeliveryChargesValue.text        = "₹ " + String(describing: detail.deliveryCharges != "" ? detail.deliveryCharges! : "0")
            lblFreeDeliveryCharges.text         = detail.deliveryCharges != "" ? "" : "FREE"
        }
    }
    
    func setStatusBorder(label: UILabel) {
        if label.text?.uppercased() == "NOT PAID" {
            label.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
            label.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
        } else if label.text?.uppercased() == "PAID" {
            label.textColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1)
            label.layer.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        } else if label.text?.uppercased() == "DELIVERED" {
            label.textColor  = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1)
            label.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        } else if label.text == "" {
            label.textColor  = UIColor.clear
            label.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func getOrderDetails() {
        let urlString = APIConstants.deliveryOrderDetail+"orderUID=\(selectedOrderUID)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.orderDetails = []
            if success {
                if Response.count > 0 {
                    Response.forEach { (item) in
                        let KeysArray = item.fieldLookup
                        let fieldsArray = item.fields
                        
                        let commonProduct = Response.filter{$0.fields[$0.fieldLookup["orderUID"].intValue].stringValue == fieldsArray[KeysArray["orderUID"].intValue].stringValue}
                        
                        self.orderDetails.append(OrderData(firstName: fieldsArray[KeysArray["firstName"].intValue].stringValue, lastName: fieldsArray[KeysArray["lastName"].intValue].stringValue, phoneNo: fieldsArray[KeysArray["phoneNo"].intValue].stringValue, note: fieldsArray[KeysArray["note"].intValue].stringValue, totalDiscount: fieldsArray[KeysArray["totalDiscount"].intValue].intValue, orderUID: fieldsArray[KeysArray["orderUID"].intValue].stringValue, orderSubtotal: fieldsArray[KeysArray["orderSubtotal"].intValue].stringValue, orderTotal: fieldsArray[KeysArray["orderTotal"].intValue].stringValue, orderDate: fieldsArray[KeysArray["orderDate"].intValue].stringValue, deliveryDate: fieldsArray[KeysArray["deliveryDate"].intValue].stringValue, orderStatus: fieldsArray[KeysArray["orderStatus"].intValue].stringValue, deliveryStatus: fieldsArray[KeysArray["deliveryStatus"].intValue].stringValue, shippingAddress: fieldsArray[KeysArray["shippingAddress"].intValue].stringValue, house: fieldsArray[KeysArray["house"].intValue].stringValue, deliveryCharges: fieldsArray[KeysArray["deliveryCharges"].intValue].stringValue, productName: commonProduct.map{$0.fields[$0.fieldLookup["productName"].intValue].stringValue}, quantityPurchased: commonProduct.map{$0.fields[$0.fieldLookup["quantityPurchased"].intValue]["low"].stringValue}, variationTitle: commonProduct.map{$0.fields[$0.fieldLookup["variationTitle"].intValue].stringValue}, discountPrice: commonProduct.map{$0.fields[$0.fieldLookup["discountPrice"].intValue].stringValue}, couponUsed: fieldsArray[KeysArray["couponUsed"].intValue].stringValue, couponDiscount: fieldsArray[KeysArray["couponDiscount"].intValue].stringValue, loyaltyPointsUsed: fieldsArray[KeysArray["loyaltyPointsUsed"].intValue].stringValue, creditsUsed: fieldsArray[KeysArray["creditsUsed"].intValue].stringValue))
                    }
                    self.updateUI()
                }
            } else {
                ReusableManager.sharedInstance.showAlert(alertString: "Failed, Please try again.")
            }
            self.tableView.reloadData()
        }
    }
}

extension String
{
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date!)
    }
}
