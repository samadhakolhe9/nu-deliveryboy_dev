//
//  MyOrdersViewController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 13/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyOrdersViewController: UIViewController {

    @IBOutlet weak var tblMyOrders: UITableView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var labelPageTitle: UILabel!
    @IBOutlet weak var labelSelectDate: UILabel!
    @IBOutlet weak var btnDateSelect: UIButton!
    @IBOutlet weak var btnOrder: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    
    struct OrderList {
        var orderUID: String!
        var name: String!
        var phoneNo: String!
        var total: Int!
        var address: String!
        var paymentStatus: String!
        var deliveryStatus: String!
    }
    
    var OrderListData:[OrderList] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMyOrders.delegate = self
        tblMyOrders.dataSource = self
        tblMyOrders.estimatedRowHeight = 50
        tblMyOrders.rowHeight = UITableView.automaticDimension
        self.tblMyOrders.separatorStyle = UITableViewCell.SeparatorStyle.none

        progressView.layer.cornerRadius = progressView.bounds.height/2
        progressView.clipsToBounds = false
        progressView.layer.masksToBounds = false
        
        btnOrder.layer.cornerRadius = 5
        btnOrder.backgroundColor = .white
        btnOrder.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        btnOrder.layer.borderWidth = 1
        btnOrder.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        
        btnMap.layer.cornerRadius = 5
        btnMap.backgroundColor = .white
        btnMap.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        btnMap.layer.borderWidth = 1
        btnMap.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAllOrders()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    @IBAction func btnOrderPressed(_ sender: UIButton) {
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "idInvoiceViewController") as! InvoiceViewController
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnMapPressed(_ sender: UIButton) {
    }
    
    func setStatusBorder(label: UILabel) {
        if label.text?.uppercased() == "NOT PAID" {
            label.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
            label.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
        } else if label.text?.uppercased() == "PAID" {
            label.textColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1)
            label.layer.borderColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
        } else if label.text?.uppercased() == "DELIVERED" {
            label.textColor  = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1)
            label.layer.borderColor = UIColor(red: 0, green: 0.858, blue: 0.807, alpha: 1).cgColor
        } else if label.text?.uppercased() == "NOT DELIVERED" {
            label.textColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1)
            label.layer.borderColor = UIColor(red: 0.914, green: 0.024, blue: 0.024, alpha: 1).cgColor
        }else if label.text == "" {
            label.textColor  = UIColor.clear
            label.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    func getAllOrders() {
        let urlString = APIConstants.deliveryOrders+"phoneNo=7066039627&date=14-10-20"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.OrderListData = []
            if success {
                if Response.count > 0 {
                    for item in Response {
                        let KeysArray = item.fieldLookup
                        let fieldsArray = item.fields
                        
                        self.OrderListData.append(OrderList(orderUID: fieldsArray[KeysArray["orderUID"].intValue].stringValue, name: fieldsArray[KeysArray["name"].intValue].stringValue, phoneNo: fieldsArray[KeysArray["phoneNo"].intValue].stringValue, total: fieldsArray[KeysArray["total"].intValue].intValue, address: fieldsArray[KeysArray["address"].intValue].stringValue, paymentStatus: fieldsArray[KeysArray["paymentStatus"].intValue].stringValue, deliveryStatus: fieldsArray[KeysArray["deliveryStatus"].intValue].stringValue))
                    }
                }
            } else {
                ReusableManager.sharedInstance.showAlert(alertString: "Failed, Please try again.")
            }
            self.tblMyOrders.reloadData()
        }
    }
}

//TODO:: TableView Delegate and DataSource Methods
extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OrderListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "idMyOrdersTableViewCell") as? MyOrdersTableViewCell else {
            return UITableViewCell()
        }
        
        if OrderListData[indexPath.row].paymentStatus == "PAID" {
            cell.leftIndicationBar.backgroundColor = UIColor(hex: "5AC571")
        }  else {
            cell.leftIndicationBar.backgroundColor = UIColor(hex: "DF4131")
        }
        
        cell.lblCustomerName.text           = OrderListData[indexPath.row].name
        cell.lblCustomerMobile.text         = OrderListData[indexPath.row].phoneNo
        cell.lblTotalAmount.text            = "TOTAL: ₹\(String(describing: OrderListData[indexPath.row].total!))"
        cell.lblShippingAddress.text        = OrderListData[indexPath.row].address
        cell.lblPaymentDeliveryStatus.text  = OrderListData[indexPath.row].paymentStatus
        cell.lblDeliveryStatus.text         = OrderListData[indexPath.row].deliveryStatus
        
        self.setStatusBorder(label: cell.lblDeliveryStatus)
        self.setStatusBorder(label: cell.lblPaymentDeliveryStatus)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "idInvoiceViewController") as! InvoiceViewController
        vc.selectedOrderUID = OrderListData[indexPath.row].orderUID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
