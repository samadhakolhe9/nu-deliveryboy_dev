//
//  VerifyOTPController.swift
//  NisargaUrja
//
//  Created by Samadhan Kolhe on 01/08/20.
//  Copyright © 2020 Samadhan Kolhe. All rights reserved.
//

import UIKit

class VerifyOTPController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imgAppIcon: UIImageView!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var btnVerifyOTP: UIButton!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var lblTimeRemains: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var mobileNumber = ""
    var isSMS = false
    
    var timer: Timer?
     var totalTime = 60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnVerifyOTP.applyCommonShadow(shadowColor: .gray, backgroundColor: UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1), radius: btnVerifyOTP.bounds.height/2)
//        btnVerifyOTP.layer.cornerRadius = 5
//        btnVerifyOTP.backgroundColor = .white
//        btnVerifyOTP.layer.backgroundColor = UIColor(red: 0.337, green: 0.761, blue: 0, alpha: 1).cgColor
//        btnVerifyOTP.layer.borderWidth = 1
//        btnVerifyOTP.layer.borderColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1).cgColor
        
        btnResendOTP.titleLabel!.textAlignment = .center
        btnResendOTP.titleLabel!.attributedText = NSMutableAttributedString(string: "Resend OTP", attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.kern: 2.7])
        
        btnBack.titleLabel!.textAlignment = .left
        btnBack.titleLabel!.attributedText = NSMutableAttributedString(string: "< Back", attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.kern: 2.7])
        btnResendOTP.alpha = 0.5
        btnResendOTP.isEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startOtpTimer()
        ReusableManager.sharedInstance.showSuccessAlert(alertString: "Your OTP has been sent on '\(isSMS ? "SMS-" : "Whatsapp.-")\(mobileNumber)'.")
    }
    
    @IBAction func BackButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func VerifyOTPButtonAction(_ sender: UIButton) {
        let urlString = APIConstants.validateOTP+"phoneNo=\(mobileNumber)&otp=\(txtOTP.text!)"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            if success {
                LocalDB.shared.setUserMobileNumber(phone: self.mobileNumber)
                let controller = self.storyboard?.instantiateViewController(withIdentifier: "idMyOrdersViewController") as! MyOrdersViewController
                self.navigationController?.pushViewController(controller, animated: true)
            } else {
                ReusableManager.sharedInstance.showSuccessAlert(alertString: "Invalid, Please try again.")
            }
        }
    }
    
    @IBAction func ResendOTPButtonAction(_ sender: UIButton) {
        let urlString = APIConstants.generateOTP+"phoneNo=\(mobileNumber)&otpmode=\(isSMS ? "SMS" : "whatsapp")"
        ApiManager().getRequestApi(url: urlString, runLoader: true, showError: false) { (Response, success) in
            self.startOtpTimer()
            ReusableManager.sharedInstance.showSuccessAlert(alertString: "Your OTP has been sent on '\(self.isSMS ? "SMS-" : "Whatsapp.-")\(self.mobileNumber)'.")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
     private func startOtpTimer() {
            self.totalTime = 60 * 5
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }
    
    @objc func updateTimer() {
            self.lblTimeRemains.text = "Time remaining: "+self.timeFormatted(self.totalTime) // will show timer
            if totalTime != 0 {
                totalTime -= 1  // decrease counter timer
            } else {
                if let timer = self.timer {
                    timer.invalidate()
                    self.timer = nil
                    self.lblTimeRemains.text = "OTP expired, Resend OTP."
                    btnResendOTP.alpha = 1.0
                    btnResendOTP.isEnabled = true
                }
            }
        }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
}
